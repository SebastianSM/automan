<?php

namespace AutoMan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Usuario extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre','apellido','correo'];

    public function vehiculos()
    {
        return $this->hasMany(Vehiculo::class);
    }

}
