<?php

namespace AutoMan;

use Illuminate\Database\Eloquent\Model;

class Dueño extends Model
{
    public function vehiculo()
    {
        return $this->belongsTo(Vehiculo::class);
    }
}
