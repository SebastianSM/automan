<?php

namespace AutoMan\Http\Controllers;

use Illuminate\Http\Request;
use AutoMan\Vehiculo;
use AutoMan\Usuario;
use AutoMan\Dueño;
use AutoMan\Precio;

class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehiculos = Vehiculo::all();

        return view('vehiculos.index', compact('vehiculos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dueños = Usuario::pluck('nombre','id');

        return view('vehiculos.create', compact('dueños'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validar = $request->validate([
            'marca' => 'required',
            'modelo' => 'required',
            'dueño' => 'required',
            'precio' => 'required',
            'año' => 'required'
        ]);
        $vehiculo = new Vehiculo();
        $vehiculo->marca = $request->input('marca');
        $vehiculo->modelo = $request->input('modelo');
        $vehiculo->año = $request->input('año');
        $vehiculo->dueño = $request->input('dueño');
        $vehiculo->precio = $request->input('precio');
        $vehiculo->save();

        $dueño = new Dueño();
        $dueño->vehiculo = $vehiculo->id; 
        $dueño->nombre = $request->input('dueño');
        $dueño->save();

        $precio= new Precio();
        $precio->vehiculo = $vehiculo->id;
        $precio->precio = $request->input('precio');
        $precio->save();

        $vehiculos = Vehiculo::all();
        return view('vehiculos.index', compact('vehiculos'));   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vehiculo $vehiculo)
    {
        $usuario = Usuario::findOrFail($vehiculo->dueño);
        $dueños = Dueño::all();
        $precios = Precio::all();

        foreach($dueños as $dueño){
            $dueño->nombre = Usuario::findOrFail($dueño->nombre)->nombre;
        }
        
        return view('vehiculos.show', compact('vehiculo','dueños','precios','usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehiculo $vehiculo)
    {
        $dueños = Usuario::pluck('nombre','id');
        return view('vehiculos.edit', compact('vehiculo','dueños'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehiculo $vehiculo)
    {
        $validar = $request->validate([
            'marca' => 'required',
            'modelo' => 'required',
            'dueño' => 'required',
            'precio' => 'required',
            'año' => 'required'
        ]);
        
        if($request->dueño != $vehiculo->dueño){
            $dueño = new Dueño();
            $dueño->vehiculo = $vehiculo->id; 
            $dueño->nombre = $request->input('dueño');
            $dueño->save();
        }

        if($request->precio != $vehiculo->precio){
            $precio= new Precio();
            $precio->vehiculo = $vehiculo->id;
            $precio->precio = $request->input('precio');
            $precio->save();
        }
        
        $vehiculo->fill($request->all());
        $vehiculo->save();

        $vehiculos = Vehiculo::all();
        return view('vehiculos.index', compact('vehiculos'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehiculo $vehiculo)
    {
        $vehiculo->delete();

        $vehiculos = Vehiculo::all();
        return view('vehiculos.index', compact('vehiculos'));
    }
}
