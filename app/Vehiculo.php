<?php

namespace AutoMan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Vehiculo extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['marca','modelo','año','dueño','precio'];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }

    public function precios()
    {
        return $this->hasMany(Precio::class);
    }

    public function dueños()
    {
        return $this->hasMany(Dueño::class);
    }
}
