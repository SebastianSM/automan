@extends('layouts.app')

@section('title' , 'Vehículos')

@section('content')
    
    <p>Listado de Vehículos</p>
    
    @foreach($vehiculos as $vehiculo)
        <div class="col-sm-6">
            <div class="card" style="width: 20rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$vehiculo->marca}} {{$vehiculo->modelo}}</h5>
                    
                    <a href="/vehiculos/{{$vehiculo->id}}" class="btn btn-primary">Datos del vehiculo</a>
                    
                    {!! Form::open(['route' => ['vehiculos.destroy', $vehiculo->id], 'method' => 'DELETE'])!!}
                        {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>

    @endforeach

@endsection