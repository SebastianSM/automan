@extends('layouts.app')

@section('title' , 'Editar Vehículo')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($vehiculo, ['route' => ['vehiculos.update', $vehiculo], 'method' => 'PUT']) !!}
    <div class="form-group">
        {!! Form::label('marca','Marca') !!}
        {!! Form::text('marca', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('modelo','Modelo') !!}
        {!! Form::text('modelo', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('año','Año') !!}
        {!! Form::number('año', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('dueño','Dueño') !!}
        {!! Form::select('dueño', $dueños, null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('precio','Precio') !!}
        {!! Form::number('precio', null, ['class' => 'form-control' ]) !!}
    </div>
        {!! Form::submit('Actualizar', ['class' => 'btn btn-primary' ]) !!}
    {!! Form::close() !!}

@endsection