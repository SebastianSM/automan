@extends('layouts.app')

@section('title' , 'Vehiculo')

@section('content')
    
    <p>{{$vehiculo->marca}} {{$vehiculo->modelo}}</p>
    
    
    <div class="row">
        <div class="col-sm-3">            
            <div class="card" style="width: 18rem;">
                <div class="form-group">
                        <label for="">Marca</label>
                        <p>{{$vehiculo->marca}}</p>
                        <label for="">Modelo</label>
                        <p>{{$vehiculo->modelo}}</p>
                        <label for="">Año</label>
                        <p>{{$vehiculo->año}}</p>
                        <label for="">Dueño</label>
                        <p>{{$usuario->nombre}} {{$usuario->apellido}}</p>
                        <label for="">Precio</label>
                        <p>${{$vehiculo->precio}}</p>
                    
                        <a href="/vehiculos/{{$vehiculo->id}}/edit" class="btn btn-primary">Editar Datos del vehiculo</a>
                </div>
            </div>
        </div>
           
        <div class="col-sm-3 offset-sm-1">    
            <div class="card" style="width: 18rem;">
                <label for="">Historial de Precios</label>  
                @foreach($precios as $precio)
                    @if($precio->vehiculo == $vehiculo->id )
                        <p>${{$precio->precio}} Desde: {{$precio->created_at}}</p>
                    @endif
                @endforeach
                
            </div>
        </div>

        <div class="col-sm-3 offset-sm-1">   
            <div class="card" style="width: 18rem;">
                <label for="">Historial de Dueños</label>
                @foreach($dueños as $dueño)
                    @if($dueño->vehiculo == $vehiculo->id)
                        <p>{{$dueño->nombre}} Desde: {{$dueño->created_at}}</p>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    

@endsection