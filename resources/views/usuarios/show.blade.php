@extends('layouts.app')

@section('title' , 'Usuario')

@section('content')
    
    <p>{{$usuario->nombre}} {{$usuario->apellido}}</p>
    
    
    <div class="row">
        <div class="col-sm">
            <div class="card" style="width: 18rem;">
                <div class="form-group">
                        <label for="">Nombre</label>
                        <p>{{$usuario->nombre}}</p>
                        <label for="">Apellido</label>
                        <p>{{$usuario->apellido}}</p>
                        <label for="">Email</label>
                        <p>{{$usuario->correo}}</p>


                        <a href="/usuarios/{{$usuario->id}}/edit" class="btn btn-primary">Editar Datos del usuario</a>

                </div>
            </div>
        </div>

    </div>
    

@endsection