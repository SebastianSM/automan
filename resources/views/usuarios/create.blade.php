@extends('layouts.app')

@section('title' , 'Crear Usuario')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(['route' => 'usuarios.store', 'method' => 'POST']) !!}
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('apellido','Apellido') !!}
        {!! Form::text('apellido', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('correo','Correo') !!}
        {!! Form::text('correo', null, ['class' => 'form-control' ]) !!}
    </div>
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary' ]) !!}
    {!! Form::close() !!}

@endsection