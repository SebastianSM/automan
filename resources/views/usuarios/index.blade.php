@extends('layouts.app')

@section('title' , 'Usuarios')

@section('content')
    
    <p>Listado de Usuarios</p>
    
    @foreach($usuarios as $usuario)
        <div class="col-sm-6 ">
            <div class="card" style="width: 20rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$usuario->nombre}} {{$usuario->apellido}}</h5>
                    <a href="/usuarios/{{$usuario->id}}" class="btn btn-primary">Datos del usuario</a>
                    
                    {!! Form::open(['route' => ['usuarios.destroy', $usuario->id], 'method' => 'DELETE'])!!}
                        {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>

    @endforeach

@endsection