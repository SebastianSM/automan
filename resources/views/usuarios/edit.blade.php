@extends('layouts.app')

@section('title' , 'Editar Usuario')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($usuario, ['route' => ['usuarios.update', $usuario], 'method' => 'PUT']) !!}
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('apellido','Apellido') !!}
        {!! Form::text('apellido', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('correo','Correo') !!}
        {!! Form::text('correo', null, ['class' => 'form-control' ]) !!}
    </div>
        {!! Form::submit('Actualizar', ['class' => 'btn btn-primary' ]) !!}
    {!! Form::close() !!}

@endsection