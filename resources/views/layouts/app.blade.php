<html>
    <head>
        <title>AutoMan - @yield('title')</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="{{ asset('css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


    </head>
    <body>
            <!-- Sidebar -->
        <div class="wrapper">
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3><i class="fas fa-tachometer-alt"></i> AutoMan</h3>
                </div>
        
                <ul class="list-unstyled components">
                    <p>Administrador de usuarios y vehículos</p>
                    <li>
                        <h4><a href="{{ url('/usuarios') }}"><i class="fas fa-users"></i> Usuarios</a></h4>
                    </li>
                    <li>
                        <a href="{{ url('/usuarios/create') }}"><i class="fas fa-user"></i> Registrar Usuario</a>
                    </li>
                    <li>
                        <h4><a href="{{ url('/vehiculos') }}"><i class="fas fa-car-side"></i> Vehículos</a></h4>
                    </li>
                    <li>
                        <a href="{{ url('/vehiculos/create') }}"><i class="fas fa-car"></i> Registrar Vehículo</a>
                    </li>
                </ul>
        
            </nav>
            <!-- Contenido -->
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    
                </nav>
                @yield('content')
            </div>
        </div>
    </body>
</html>